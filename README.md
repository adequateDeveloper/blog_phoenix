# BlogPhoenix

Features:

  * Configurations for SemaphoreCI and Heroku
  * Unit tests for Model and Controller

[![Build Status](https://semaphoreci.com/api/v1/adequateDeveloper/blog_phoenix/branches/master/badge.svg)](https://semaphoreci.com/adequateDeveloper/blog_phoenix)
